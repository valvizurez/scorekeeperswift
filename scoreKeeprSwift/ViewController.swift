//
//  ViewController.swift
//  scoreKeeprSwift
//
//  Created by Victoria Alvizurez on 9/5/17.
//  Copyright © 2017 Victoria Alvizurez. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet var team1Lbl: UILabel!
    @IBOutlet var team2Lbl: UILabel!
    
    @IBOutlet var Team2View: UIView!
    @IBOutlet var Team1View: UIView!
    
    @IBOutlet var team1text: UITextField!
    @IBOutlet var team2text: UITextField!
    
    @IBOutlet var team1Stepper: UIStepper!
    @IBOutlet var team2Stepper: UIStepper!
    
   
    @IBAction func team2Stepper(_ sender: Any) {
        let myString = String(format: "%.0f", team2Stepper.value)
        team2Lbl.text = myString

    }
    
  
    @IBAction func team1StepperChanged(_ sender: Any) {
        let myString = String(format: "%.0f", team1Stepper.value)
        team1Lbl.text = myString
    }
    
 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.team1text.delegate = self
        self.team2text.delegate = self

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        team1text.resignFirstResponder()
        team2text.resignFirstResponder()
        return true

    }
    
    @IBAction func resetBtnTouched(_ sender: Any) {
        //lables to zero
        //steppers to zero
        
        team1Stepper.value = 0;
        team1Lbl.text = "0";
        
        team2Stepper.value = 0;
        team2Lbl.text = "0";
        
        team1text.text = "";
        team2text.text = "";
        
    }
}

